#Bluechip front web template#

##Description##
A front end web application template/build with angularJS, sass, gulp.

##Requirements##
1. NPM
2. Bower
3. Sass

##Dependencies##
1. angularJS
2. JQuery
3. Anything in the package.json file
4. Anything in the bower.json file

##Folder Structure##

###Root###
1. Contains package.json file with all packages and description for build
2. Gulp file - tasks that build , watch and serve distribution and development builds where necessary
3. Gulpfile.config - Contains necessary file configs for the gulp file
4. bower.json - contains all dependencies for the actual application you're building. I seperated it from npm just my preference
5. bowerrc - config for bower dependencies. primarily just to specify where the bower components are installed

###src - Your development folder###
Contains all parts of your app

1. app/scrips - contains your custom scripts for your app
2. app/ - contains your angular route configuration (if needed)
3. assets/ - contains any assets you may need for your app
4. css/ - contains created css files from gulp task (You can config this by editing the gulp file)
5. js/ - contains js files you may need
6. scss/ - contains sass files to be compiled
7. views/ - contains html templates for your application (you may change this based on your preference)