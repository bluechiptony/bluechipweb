module.exports = function(){
    var client = './src',
        dist = './dist/',
        angularApp = './src/app',
        tmp = 'tmp';
    var config = {
        source : client,
        dist : dist,
        index: client+'/index.html',
        dests:{
            css:client+'/css'
        },
        appJs:[
            angularApp+'/**/*.js',
            //'!'+angularApp+'/**/echarts.js'
        ],
        sass:[
            client+'/scss/base.scss',
            client+'/scss/dovan.scss'
        ],
        css:[
            client+'/css/**/*.css'
        ],
        html:[
            client+'/**/*.html'
        ],
        assets:[

        ],
        preDist:[
            client+'/**/*.html',
            '!'+client+'/bower_components'
        ]



    }

    return config;
}
