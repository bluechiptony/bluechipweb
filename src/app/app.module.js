(function() {
    angular.module("app", [
        'ui.bootstrap',
        'ui.router',
        'ngAnimate',
        'ngCookies',
        'carousel'
        //'route.config'
    ]);
})();
