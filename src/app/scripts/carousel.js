(function(){
    angular.module("carousel", [])
    .controller("carouselController", ["$scope", "$http", carCtrl]);

    function carCtrl($scope, $http){
        $scope.interval = 3000;
        $scope.noWrapSlides =false;
        $scope.active = 0;
        $scope.slides = [
          {id:0, caption:'Cusromer Service At its best',
            imageURL:'../assets/imgs/customer-smile.png'},
          {id:1, caption:'Focusing on your brand',
            imageURL:'../assets/imgs/advert.jpg'},
          {id:2, caption:'providing technical support',
            imageURL:'../assets/imgs/support.jpg'},
        ];
    }
})();
