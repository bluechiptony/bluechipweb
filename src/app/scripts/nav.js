$(document).ready(function(){

    $("#hom").click(function(e){
        e.preventDefault();

        $('html,body').animate({
            scrollTop: $("#carousel").offset().top
        }, 'slow');
    });
    $("#abt").click(function(e){
        e.preventDefault();

        $('html,body').animate({
            scrollTop: $("#about").offset().top
        }, 'slow');
    });
    $("#serv").click(function(e){
        e.preventDefault();

        $('html,body').animate({
            scrollTop: $("#services").offset().top
        }, 'slow');
    });
    $("#cli").click(function(e){
        e.preventDefault();

        $('html,body').animate({
            scrollTop: $("#clients").offset().top
        }, 'slow');
    });
    $("#con").click(function(e){
        //e.preventDefault();

        $('html,body').animate({
            scrollTop: $("#contact").offset().top
        }, 'slow');
    });

    $(window).scroll(function(){
        //console.log("scroll");
        if(window.scrollY >= $("#about").offset().top ){
            $("#nav-holder-container").css('background', '#fff');
            $("#nav").removeClass('navigation-list-carousel');
            $("#nav").addClass('navigation-list');
            $("#social-nav").removeClass('navigation-list-carousel');
            $("#social-nav").addClass('navigation-list');
        }else{
            $("#nav-holder-container").css('background', 'none');
            $("#nav").removeClass('navigation-list');
            $("#nav").addClass('navigation-list-carousel');
            $("#social-nav").removeClass('navigation-list');
            $("#social-nav").addClass('navigation-list-carousel');
        }

    });

});
